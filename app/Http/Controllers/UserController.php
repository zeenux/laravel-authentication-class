<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function index(){
        $roles=Role::all();
        return view('users.index')->with('users',User::all())->with('roles',$roles);
    }

    public function yahoo(){
        return view('users.yahoo');
    }
}
